"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'dyplom.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.Group(
            _('Glowne menu'),
            column=1,
            collapsible=True,
            children = [
                modules.ModelList(
                    column=1,
                    css_classes=('collapse closed',),
                    models=('django.contrib.auth.models.Group',
                            'entries.models.Contact'),
                )
            ]
        ))

        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            _('Publikacje'),
            column=1,
            collapsible=True,
            children = [
                modules.ModelList(
                    column=1,
                    collapsible=False,
                    models=('entries.models.Category',
                            'entries.models.Entry'),
                ),
            ]
        ))

        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            _('Uzytkownik'),
            column=1,
            collapsible=True,
            children = [
                modules.ModelList(
                    column=1,
                    css_classes=('collapse closed',),
                    models=('userprofiles.models.Files',
                            'userprofiles.models.UserProfiles'),
                )
            ]
        ))

        if context['user'].is_superuser:
            self.children.append(modules.LinkList(
                _('Media'),
                column=2,
                children=[
                    {
                        'title': _('FileBrowser'),
                        'url': '/admin/filebrowser/browse/',
                        'external': False,
                    },
                ]
            ))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Linki'),
            column=2,
            children=[
                {
                    'title': _('Strona glowna'),
                    'url': '/',
                    'external': True,
                },
                {
                    'title': _('Strona UwB'),
                    'url': 'http://www.uwb.edu.pl/',
                    'external': True,
                },
            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))

