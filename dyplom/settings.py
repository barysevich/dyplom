"""
Django settings for dyplom project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5u91sj)a2%)($qliq5$%^h2*7635+2z57c^uwg(hi=-&fnrss+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

THUMBNAIL_DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    # 'grappelli.dashboard',
    'gipsy.dashboard',
    'gipsy.toolbar',

    'grappelli',
    'filebrowser',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'sorl.thumbnail',

    'userprofiles',
    'entries',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.i18n",
    'django.contrib.messages.context_processors.messages',
)

ROOT_URLCONF = 'dyplom.urls'

WSGI_APPLICATION = 'dyplom.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'Europe/Warsaw'

USE_I18N = True

USE_L10N = True

USE_TZ = True

AUTH_USER_MODEL = 'userprofiles.UserProfiles'

# GIPSY_DASHBOARD = 'dyplom.dashboard.CustomIndexDashboard'
#
# GOOGLE_ANALYTICS_CLIENT_SECRETS = ''
# GOOGLE_ANALYTICS_TOKEN_FILE_NAME = ''
# GOOGLE_ANALYTICS_VIEW_ID = ''
#
GRAPPELLI_INDEX_DASHBOARD = 'dyplom.dashboard.CustomIndexDashboard'

GRAPPELLI_ADMIN_TITLE = 'Uniwersytet w Bialymstoku'

MEDIA_URL = '/media/'

MEDIA_ROOT = (
    BASE_DIR + '/media')

STATIC_ROOT = 'staticfiles'

STATIC_URL = '/staticfiles/'

STATICFILES_DIRS = (
    BASE_DIR + '/static/',
)

TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
)

try:
    from .local_settings import *
except ImportError:
    print "import settings error"