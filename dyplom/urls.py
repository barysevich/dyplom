from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.decorators.http import require_POST
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from filebrowser.sites import site

from userprofiles.views import UserProfileDetailView, \
    RegistrationView, RegSuccessView
from entries.views import CategoryDetailView, \
    EntryDetailView, ContactView, ThanksView, \
    IndexPageView


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', IndexPageView.as_view(), name='index'),

    url(r'^(?P<slug>[\w-]+)/$',
        UserProfileDetailView.as_view(),
        name='profile'),

    url(r'^category/(?P<slug>[\w-]+)/$',
        CategoryDetailView.as_view(),
        name='category'),
    url(r'^category/(?P<slug>[\w-]+)/(?P<pk>\d+)/$',
        EntryDetailView.as_view(),
        name='entries'),

    url(r'^page/reg/$',
        RegistrationView.as_view(),
        name='reg'),

    url(r'^page/contact/$',
        require_POST(ContactView.as_view()),
        name='contact'),

    url('^page/thanks/$',
        ThanksView.as_view(),
        name='thanks'),

    url('^page/reg/success/$',
        RegSuccessView.as_view(),
        name='reg-success'),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.STATIC_ROOT,
            }),
    )

urlpatterns += staticfiles_urlpatterns()