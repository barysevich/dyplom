from django.contrib import admin
from django.http import Http404

from .models import Category, Entry, Contact
from userprofiles.models import UserProfiles


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'text',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'slug',
                    'list_number', 'active',)
    list_editable = ('active', 'list_number')
    prepopulated_fields = {'slug': ('name',)}
    ordering = ['list_number']

    fieldsets = (
        ('Nazwa', {'fields': ('name', 'slug')}),
        ('Info', {'fields': ('image', 'desc', 'active')}),
    )

    def get_queryset(self, request):
        qs = Category.objects.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(author__pk=request.user.pk)

    def save_model(self, request, obj, form, change):
        obj.save()

        if not change:
            try:
                user = UserProfiles.objects.get(pk=request.user.pk)
            except UserProfiles.DoesNotExist:
                raise Http404
            obj.author = user
            obj.save()

    class Media:
        js = (
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        )



@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('name', 'date', 'category', 'list_number',
                    'active',)
    list_editable = ('list_number', 'active',)
    search_fields = ('name',)
    ordering = ['list_number']

    fieldsets = (
        ('Nazwa', {'fields': ('name', 'category',)}),
        ('Obrazki', {'fields': ('image', 'banner',)}),
        ('Opis', {'fields': ('desc', 'text',)}),
        ('Info', {'fields': ('list_number', 'active')}),
    )

    class Media:
        js = (
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        )

    def get_queryset(self, request):
        qs = Entry.objects.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(category__author__pk=request.user.pk)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            context['adminform'].form.fields['category'].queryset = Category.objects.filter(
                author__pk=request.user.pk
            )
        return super(EntryAdmin, self).render_change_form(
            request, context, args, kwargs
        )
