from django import forms

from .models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ('name', 'email', 'text')
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Tytul', 'class':
                'form-control', 'required': 'required'}),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email', 'class':
                'form-control',}),
            'text': forms.Textarea(attrs={
                'placeholder': 'Wiadomosc', 'rows': '5',
                'class': 'form-control', 'required': 'required'}),
        }
