# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, null=True, verbose_name='Nazwa kategorii')),
                ('slug', models.SlugField(max_length=256, null=True, verbose_name='Link do kategorii')),
                ('active', models.BooleanField(default=True, verbose_name='Czy pokazywac kategorie?')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, null=True, verbose_name='Nazwa artykula')),
                ('text', models.TextField(verbose_name='Text')),
                ('list_number', models.PositiveSmallIntegerField(default=0, verbose_name='Sortowanie')),
                ('active', models.BooleanField(default=True, verbose_name='Czy pokazywac ten artykul?')),
                ('category', models.ForeignKey(to='entries.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
