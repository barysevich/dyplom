# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Categoria', 'verbose_name_plural': 'Categorie'},
        ),
        migrations.AlterModelOptions(
            name='entry',
            options={'ordering': ['list_number'], 'verbose_name': 'Artykul', 'verbose_name_plural': 'Artykuly'},
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(max_length=256, unique=True, null=True, verbose_name='Link do kategorii'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='list_number',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Sortowanie(n.p. 1,2,...)'),
            preserve_default=True,
        ),
    ]
