# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0002_auto_20150510_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='list_number',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Sortowanie'),
            preserve_default=True,
        ),
    ]
