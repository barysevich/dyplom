# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0003_auto_20150510_2116'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, null=True, verbose_name='Nazwa wiadomosci')),
                ('email', models.EmailField(max_length=256, null=True, verbose_name='Email', blank=True)),
                ('text', models.TextField(null=True, verbose_name='Wiadomosc')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
