# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0004_contact'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'ordering': ['date'], 'verbose_name': 'Kontakt', 'verbose_name_plural': 'Kontakty'},
        ),
        migrations.AddField(
            model_name='category',
            name='desc',
            field=models.TextField(null=True, verbose_name='Opis kategorii', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Zdjecie kategorii', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contact',
            name='date',
            field=models.DateField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
