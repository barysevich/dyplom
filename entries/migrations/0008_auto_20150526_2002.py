# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0007_category_author'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'verbose_name': 'Wiadomosc od uzytkownika', 'verbose_name_plural': 'Wiadomosci od uzytkownikow'},
        ),
        migrations.AlterField(
            model_name='category',
            name='active',
            field=models.BooleanField(default=True, help_text=b'<h4>Jezeli jest zaznaczone, to do tego artykula jest dostep</h4>', verbose_name='Czy pokazywac kategorie?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='author',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, help_text=b'Author categorii', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(null=True, max_length=256, help_text=b'Jest potrzebny do generacji URL do tej kategorii,.<br> <h3>example.com/category/test</h3>url w tym przypadku jest rowny "test"', unique=True, verbose_name='Link do kategorii'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='active',
            field=models.BooleanField(default=True, help_text=b'<h4>Jezeli jest zaznaczone, to do tego artykula jest dostep</h4>', verbose_name='Pokazywac ten artykol?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='category',
            field=models.ForeignKey(help_text=b'Categoria, gdzie bedzie umieszczony artykul', to='entries.Category'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='date',
            field=models.DateField(auto_now_add=True, verbose_name='Data', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='list_number',
            field=models.PositiveSmallIntegerField(default=0, help_text=b'Prosze podac cyfre od 0 do 99', verbose_name='Kolejnosc wyswietlania'),
            preserve_default=True,
        ),
    ]
