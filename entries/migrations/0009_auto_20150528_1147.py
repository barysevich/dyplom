# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0008_auto_20150526_2002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='author',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, help_text=b'Author categorii', null=True),
            preserve_default=True,
        ),
    ]
