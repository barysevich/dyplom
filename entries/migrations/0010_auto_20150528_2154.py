# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0009_auto_20150528_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='banner',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Banner artykulu', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='entry',
            name='image',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Zdjecie artykulu', blank=True),
            preserve_default=True,
        ),
    ]
