# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0010_auto_20150528_2154'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='desc',
            field=models.CharField(max_length=512, null=True, verbose_name='Opis', blank=True),
            preserve_default=True,
        ),
    ]
