# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0011_entry_desc'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['list_number'], 'verbose_name': 'Categoria', 'verbose_name_plural': 'Categorie'},
        ),
        migrations.AddField(
            model_name='category',
            name='list_number',
            field=models.PositiveSmallIntegerField(default=0, help_text=b'Prosze podac cyfre od 0 do 99', verbose_name='Kolejnosc wyswietlania'),
            preserve_default=True,
        ),
    ]
