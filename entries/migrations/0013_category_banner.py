# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0012_auto_20150605_2012'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='banner',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Banner kategorii', blank=True),
            preserve_default=True,
        ),
    ]
