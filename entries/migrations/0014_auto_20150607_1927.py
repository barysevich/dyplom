# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0013_category_banner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='banner',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Banner kategorii'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Zdjecie kategorii'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='banner',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Banner artykulu'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='image',
            field=models.ImageField(upload_to=b'images/', null=True, verbose_name='Zdjecie artykulu'),
            preserve_default=True,
        ),
    ]
