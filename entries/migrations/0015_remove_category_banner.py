# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0014_auto_20150607_1927'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='banner',
        ),
    ]
