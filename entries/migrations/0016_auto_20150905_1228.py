# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0015_remove_category_banner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(null=True, max_length=255, help_text=b'Jest potrzebny do generacji URL do tej kategorii,.<br> <h3>example.com/category/test</h3>url w tym przypadku jest rowny "test"', unique=True, verbose_name='Link do kategorii'),
            preserve_default=True,
        ),
    ]
