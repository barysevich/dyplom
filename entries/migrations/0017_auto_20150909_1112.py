# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0016_auto_20150905_1228'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['list_number'], 'verbose_name': 'Kategoria', 'verbose_name_plural': 'Kategorie'},
        ),
    ]
