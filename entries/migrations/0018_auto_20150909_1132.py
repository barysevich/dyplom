# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0017_auto_20150909_1112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='name',
            field=models.CharField(max_length=256, null=True, verbose_name='Nazwa artykulu'),
            preserve_default=True,
        ),
    ]
