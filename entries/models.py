from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings


class Contact(models.Model):
    name = models.CharField(
        _('Nazwa wiadomosci'),
        max_length=256, null=True, blank=False)
    email = models.EmailField(
        _('Email'),
        max_length=256, null=True, blank=True)
    text = models.TextField(
        _('Wiadomosc'),
        null=True, blank=False)
    date = models.DateField(
        auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name = 'Wiadomosc od uzytkownika'
        verbose_name_plural = 'Wiadomosci od uzytkownikow'

    def __unicode__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(
        _('Nazwa kategorii'),
        max_length=256, null=True, blank=False)
    slug = models.SlugField(
        _('Link do kategorii'),
        help_text='Jest potrzebny do generacji URL do tej kategorii,'
                  '.<br> <h3>example.com/category/test</h3>'
                  'url w tym przypadku jest rowny "test"',
        max_length=255, null=True,
        blank=False, unique=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        help_text='Author categorii',
        null=True, blank=True)
    image = models.ImageField(
        _('Zdjecie kategorii'),
        upload_to='images/',
        null=True, blank=False)
    desc = models.TextField(
        _('Opis kategorii'),
        null=True, blank=True)
    list_number = models.PositiveSmallIntegerField(
        _('Kolejnosc wyswietlania'),
        help_text='Prosze podac cyfre od 0 do 99',
        default=0)
    active = models.BooleanField(
        _('Czy pokazywac kategorie?'),
        help_text='<h4>Jezeli jest zaznaczone, '
                  'to do tego artykula jest dostep</h4>',
        default=True)

    class Meta:
        ordering = ['list_number']
        verbose_name = 'Kategoria'
        verbose_name_plural = 'Kategorie'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category', args=(self.slug,))


class Entry(models.Model):
    name = models.CharField(
        _('Nazwa artykulu'),
        max_length=256, null=True, blank=False)
    image = models.ImageField(
        _('Zdjecie artykulu'),
        upload_to='images/',
        null=True, blank=False)
    banner = models.ImageField(
        _('Banner artykulu'),
        upload_to='images/',
        null=True, blank=False)
    desc = models.CharField(
        _('Opis'), max_length=512,
        blank=True, null=True)
    text = models.TextField(
        _('Text'), blank=False)
    category = models.ForeignKey(
        Category, blank=False,
        help_text='Categoria, gdzie bedzie umieszczony artykul',)
    list_number = models.PositiveSmallIntegerField(
        _('Kolejnosc wyswietlania'),
        help_text='Prosze podac cyfre od 0 do 99',
        default=0)
    date = models.DateField(
        _('Data'), auto_now_add=True,
        null=True, blank=True)
    active = models.BooleanField(
        _('Pokazywac ten artykol?'),
        help_text='<h4>Jezeli jest zaznaczone, '
                  'to do tego artykula jest dostep</h4>',
        default=True)

    class Meta:
        verbose_name = 'Artykul'
        verbose_name_plural = 'Artykuly'
        ordering = ['list_number']

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('entries', args=(self.category.slug, self.pk,))