from django.http import Http404
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView, \
    TemplateView, FormView

from userprofiles.models import UserProfiles

from .models import Category, Entry
from .forms import ContactForm


class IndexPageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context['users'] = UserProfiles.objects.filter(is_active=True, show=True)
        context['categories'] = Category.objects.filter(active=True)
        context['form'] = ContactForm()
        return context


class CategoryDetailView(DetailView):
    model = Category
    template_name = 'category.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        slug = self.kwargs.get('slug', '')
        context['entries'] = Entry.objects.filter(category__slug=slug, active=True)
        return context


class EntryDetailView(DetailView):
    model = Entry
    template_name = 'entry.html'

    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug', '')
        pk = self.kwargs.get('pk', '')

        try:
            obj = Entry.objects.get(category__slug=slug, pk=pk, active=True)
        except Entry.DoesNotExist:
            raise Http404

        return obj

    def get_context_data(self, **kwargs):
        context = super(EntryDetailView, self).get_context_data(**kwargs)
        slug = self.kwargs.get('slug', '')
        context['navigation'] = Entry.objects.filter(category__slug=slug, active=True)
        return context


class ContactView(FormView):
    form_class = ContactForm
    template_name = 'errors/400.html'
    success_url = reverse_lazy('thanks')

    def form_valid(self, form):
        form.save()
        return super(ContactView, self).form_valid(form)


class ThanksView(TemplateView):
    template_name = 'thanks.html'
