from django.contrib import admin
from django.http import Http404

from .models import UserProfiles, Files
from .forms import AdminUserAddForm, AdminUserChangeForm


@admin.register(UserProfiles)
class UserProfileAdmin(admin.ModelAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm

    list_filter = ('last_login', 'is_staff',
                   'is_active', 'show')
    list_display = ('username', 'first_name', 'last_name',
                    'email', 'last_login', 'desc', 'avatar',
                    'is_staff', 'is_active', 'show',)

    def get_queryset(self, request):
        qs = UserProfiles.objects.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(pk=request.user.pk)

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            ('Login/Haslo', {'fields': ('username',
                                        'password')}),
            ('Info', {'fields': ('avatar', 'slug', 'first_name',
                                 'last_name', 'email')}),
            ('Opis', {'fields': ('desc', 'short_desc',
                                 'mainpage_desc',)}),
        )

        if request.user.is_superuser:
            fieldsets = (
                ('Login/Haslo', {'fields': ('username',
                                            'password')}),
                ('Info', {'fields': ('avatar', 'slug', 'first_name',
                                     'last_name', 'email')}),
                ('Opis', {'fields': ('desc', 'short_desc',
                                     'mainpage_desc',)}),
                ('Dostep', {'fields': ('show', 'groups',
                                       'is_staff', 'is_active',)})
            )
        return fieldsets

    class Media:
        js = (
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
        )


@admin.register(Files)
class FilesAdmin(admin.ModelAdmin):
    list_display = ('name', 'file', 'list_number', 'show', 'icon',)
    list_editable = ('list_number', 'show', 'icon',)
    ordering = ['list_number']

    def get_queryset(self, request):
        qs = Files.objects.all_with_hidden()
        if request.user.is_superuser:
            return qs
        return qs.filter(userprofiles__pk=request.user.pk)

    def save_model(self, request, obj, form, change):
        obj.save()

        if not change:
            try:
                user = UserProfiles.objects.get(pk=request.user.pk)
            except UserProfiles.DoesNotExist:
                raise Http404
            user.files.add(obj)
            user.save()
