import factory

from .models import UserProfiles, Files


class UserFactory(factory.Factory):
    class Meta:
        model = UserProfiles

    first_name = 'Art'
    last_name = 'Bar'

class FilesFactory(factory.Factory):
    class Meta:
        model = Files

    name = 'Some file'
    show = True
    list_number = factory.Sequence(lambda n: '%s' % n)

