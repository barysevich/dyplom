from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import UserProfiles


class AdminUserAddForm(UserCreationForm):
    class Meta:
        model = UserProfiles
        fields = ('username', 'password')

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            UserProfiles._default_manager.get(username=username)
        except UserProfiles.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class AdminUserChangeForm(UserChangeForm):
    class Meta:
        model = UserProfiles
        fields = ('username',)


class RegistrationForm(UserCreationForm):
    username = forms.CharField(
        label='Nazwa uzytkownika',
        widget=forms.TextInput(attrs={
            'placeholder': 'Nazwa uzytkownika',
            'class': 'form-control',
            'required': 'required'}))
    password1 = forms.CharField(
        label='Haslo',
        widget=forms.PasswordInput(attrs={
            'placeholder': 'Haslo',
            'class': 'form-control',
            'required': 'required'}))
    password2 = forms.CharField(
        label='Powtorz haslo',
        widget=forms.PasswordInput(attrs={
            'placeholder': 'Powtorz haslo',
            'class': 'form-control',
            'required': 'required'}))

    class Meta:
        model = UserProfiles
        fields = ('username', 'password1', 'password2')
        widgets = {
            'username': forms.TextInput(attrs={
                'placeholder': 'Nazwa uzytkownika',
                'class': 'form-control',
                'required': 'required'}),
        }

    def clean_username(self):
        username = self.cleaned_data['username']

        try:
            self.Meta.model.objects.get(username=username)
        except self.Meta.model.DoesNotExist:
            return username

        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        if len(password1) < 5:
            raise forms.ValidationError(
                "Haslo jest za krotkie(minimalne 5 znakow)"
            )
        return password2
