from django.db import models


class ActiveFilesManager(models.Manager):
    def all(self):
        return self.filter(show=True)

    def all_with_hidden(self):
        return self.filter()
