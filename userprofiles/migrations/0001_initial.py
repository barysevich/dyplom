# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfiles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('slug', models.SlugField(null=True, max_length=256, blank=True, unique=True, verbose_name='url do usera')),
                ('avatar', models.ImageField(upload_to=b'profiles/', null=True, verbose_name='Zdjecie', blank=True)),
                ('desc', models.TextField(max_length=300, null=True, verbose_name='Opis', blank=True)),
            ],
            options={
                'verbose_name': 'Uzytkownik',
                'verbose_name_plural': 'Uzytkownicy',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Files',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Nazwa pliku')),
                ('file', models.FileField(upload_to=b'files/', verbose_name='Link do tego pliku')),
                ('show', models.BooleanField(default=True, help_text=b'<h4>Jezeli jest zaznaczone, to do tego pliku jest dostep</h4>', verbose_name='Pokazywac ten plik?')),
                ('list_number', models.PositiveSmallIntegerField(default=0, help_text=b'Prosze podac cyfre od 0 do 99', verbose_name='Kolejnosc wyswietlania')),
                ('icon', models.CharField(default=b'fa-file-o', max_length=32, verbose_name='Obrazek obok pliku', choices=[(b'fa-file-o', b'Standartowy'), (b'fa-file-image-o', b'Zdjecie'), (b'fa-file-pdf-o', b'Plik PDF'), (b'fa-file-text-o', b'Plik textowy'), (b'fa-file-word-o', b'Dokument WORD'), (b'fa-file-code-o', b'Plik z kodem'), (b'fa-file-archive-o', b'Zip/Rar')])),
            ],
            options={
                'ordering': ['list_number'],
                'verbose_name': 'Plik',
                'verbose_name_plural': 'Pliki',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userprofiles',
            name='files',
            field=models.ManyToManyField(to='userprofiles.Files', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofiles',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofiles',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
            preserve_default=True,
        ),
    ]
