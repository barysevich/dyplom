# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0002_userprofiles_short_desc'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofiles',
            name='show',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
