# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0003_userprofiles_show'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofiles',
            name='show',
            field=models.BooleanField(default=False, verbose_name='Pokazywac na stronie glownej?'),
            preserve_default=True,
        ),
    ]
