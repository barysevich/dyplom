# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0004_auto_20150605_2012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofiles',
            name='desc',
            field=models.TextField(max_length=5000, null=True, verbose_name='Opis', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofiles',
            name='short_desc',
            field=models.TextField(max_length=512, null=True, verbose_name='Krotki opis', blank=True),
            preserve_default=True,
        ),
    ]
