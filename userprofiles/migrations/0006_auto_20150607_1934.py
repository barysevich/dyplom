# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0005_auto_20150607_1927'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofiles',
            name='mainpage_desc',
            field=models.CharField(max_length=512, null=True, verbose_name='Opis na stronie glownyj', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofiles',
            name='desc',
            field=models.TextField(max_length=5000, null=True, verbose_name='Opis w profilu', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofiles',
            name='short_desc',
            field=models.TextField(max_length=512, null=True, verbose_name='Krotki opis w profilu', blank=True),
            preserve_default=True,
        ),
    ]
