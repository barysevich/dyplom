# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0006_auto_20150607_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofiles',
            name='mainpage_desc',
            field=models.CharField(max_length=512, null=True, verbose_name='Opis na stronie glownej', blank=True),
            preserve_default=True,
        ),
    ]
