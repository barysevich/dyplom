# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0007_auto_20150611_2205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofiles',
            name='desc',
            field=models.TextField(null=True, verbose_name='Opis w profilu', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofiles',
            name='short_desc',
            field=models.TextField(max_length=1024, null=True, verbose_name='Krotki opis w profilu', blank=True),
            preserve_default=True,
        ),
    ]
