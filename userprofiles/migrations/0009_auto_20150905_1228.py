# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofiles', '0008_auto_20150611_2228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofiles',
            name='slug',
            field=models.SlugField(null=True, max_length=255, blank=True, unique=True, verbose_name='url do usera'),
            preserve_default=True,
        ),
    ]
