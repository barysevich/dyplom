from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db import models

from .managers import ActiveFilesManager


class Files(models.Model):
    UNKNOWN = 'fa-file-o'
    IMAGE = 'fa-file-image-o'
    PDF = 'fa-file-pdf-o'
    WORD = 'fa-file-word-o'
    TEXT = 'fa-file-text-o'
    CODE = 'fa-file-code-o'
    ARCHIVE = 'fa-file-archive-o'

    ICON_CHOICES = (
        (UNKNOWN, 'Standartowy'),
        (IMAGE, 'Zdjecie'),
        (PDF, 'Plik PDF'),
        (TEXT, 'Plik textowy'),
        (WORD, 'Dokument WORD'),
        (CODE, 'Plik z kodem'),
        (ARCHIVE, 'Zip/Rar'),
    )

    name = models.CharField(
        _('Nazwa pliku'),
        max_length=255, blank=False)
    file = models.FileField(
        _('Link do tego pliku'),
        upload_to='files/')
    show = models.BooleanField(
        _('Pokazywac ten plik?'),
        help_text='<h4>Jezeli jest zaznaczone, '
                  'to do tego pliku jest dostep</h4>',
        default=True)

    list_number = models.PositiveSmallIntegerField(
        _('Kolejnosc wyswietlania'),
        help_text='Prosze podac cyfre od 0 do 99',
        default=0)

    icon = models.CharField(
        _('Obrazek obok pliku'), max_length=32,
        choices=ICON_CHOICES, default=UNKNOWN)

    objects = ActiveFilesManager()

    class Meta:
        verbose_name = 'Plik'
        verbose_name_plural = 'Pliki'
        ordering = ['list_number']

    def __unicode__(self):
        return self.name


class UserProfiles(AbstractUser):
    slug = models.SlugField(
        _('url do usera'),
        max_length=255, null=True,
        blank=True, unique=True)
    avatar = models.ImageField(
        _('Zdjecie'), upload_to='profiles/',
        null=True, blank=True)
    mainpage_desc = models.CharField(
        _('Opis na stronie glownej'),
        max_length=512,
        null=True, blank=True)
    short_desc = models.TextField(
        _('Krotki opis w profilu'),
        max_length=1024,
        null=True, blank=True)
    desc = models.TextField(
        _('Opis w profilu'),
        null=True, blank=True)
    files = models.ManyToManyField(
        Files, blank=True, null=True)
    show = models.BooleanField(
        _('Pokazywac na stronie glownej?'),
        default=False)

    class Meta:
        verbose_name = 'Uzytkownik'
        verbose_name_plural = 'Uzytkownicy'

    def get_absolute_url(self):
        return reverse('profile', args=(self.slug,))

    def show_username(self):
        if self.first_name and self.last_name:
            return '%s %s' % (self.first_name, self.last_name)
        return self.username

    def __unicode__(self):
        return self.username
