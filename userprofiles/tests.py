from django.test import TestCase, Client

from factories import UserFactory, FilesFactory


class UserProfilesTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.file = FilesFactory()

    def test_file(self):
        self.assertEqual(self.file.__unicode__(), u'Some file')

    def test_user(self):
        self.assertEqual(self.user.show_username(), u'Art Bar')

    def test_user_detail(self):
        pass

    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_categories(self):
        pass

    def test_entries(self):
        pass
