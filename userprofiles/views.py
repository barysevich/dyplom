import random

from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import FormView

from .forms import RegistrationForm
from .models import UserProfiles
from entries.models import Category


class UserProfileDetailView(DetailView):
    model = UserProfiles
    template_name = 'profile.html'

    def get_context_data(self, **kwargs):
        context = super(UserProfileDetailView, self).get_context_data(**kwargs)
        obj = kwargs.get('object', '')
        context['publications'] = Category.objects.filter(
            author__pk=obj.pk).filter(active=True)
        return context


class RegistrationView(FormView):
    form_class = RegistrationForm
    template_name = 'reg.html'
    success_url = reverse_lazy('reg-success')

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)

        context['image'] = random.choice(['1.jpg', '2.png',
                                          '3.jpg', '4.png'])
        return context


class RegSuccessView(TemplateView):
    template_name = 'reg-success.html'
